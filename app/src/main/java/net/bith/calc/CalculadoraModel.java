package net.bith.calc;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.List;

public class CalculadoraModel extends ViewModel {

    private String operador = "";

    private List<String> historial = new ArrayList<>();
    private MutableLiveData<List<String>> historialMutable = new MutableLiveData<List<String>>();

    public LiveData<List<String>> getHistorial() {
        return historialMutable;
    }
    public void addToHistorial(String operacion) {

        int num = historial.size();
        boolean duplicado = false;
        if (num>0) {
            String ultima = historial.get(num - 1);
            String[] partes = ultima.split("\\.");
            duplicado = (operacion == partes[1]);
        }
        if (!duplicado) {
            historial.add(String.format("%d. %s",num+1,operacion));
            this.historialMutable.setValue(historial);
        }
    }

    public String getOperador() {
        return operador;
    }
    public void setOperador(String operador) {
        this.operador = operador;
    }


}
