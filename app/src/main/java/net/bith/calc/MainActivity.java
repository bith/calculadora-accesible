package net.bith.calc;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private EditText operando1,operando2;
    private TextView resultado;
    private ListView historial;
    private Button botonOcultar,botonMostrar,botonCancelar;

    private CalculadoraModel modelo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        historial = (ListView) findViewById(R.id.historial);
        modelo =  new ViewModelProvider(this).get(CalculadoraModel.class);
        modelo.getHistorial().observe( this, lista -> {
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,android.R.id.text1,lista);
            historial.setAdapter(adapter);
        } );


        setContentView(R.layout.activity_main);

        resultado = (TextView) findViewById(R.id.resultado);
        operando1 = (EditText) findViewById(R.id.operando1);
        operando1.requestFocus();
        operando2 = (EditText) findViewById(R.id.operando2);
        botonOcultar = (Button) findViewById(R.id.botonOcultar);
        botonMostrar = (Button) findViewById(R.id.botonHistorial);
        botonCancelar= (Button) findViewById(R.id.botonBorrar);


        operando1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) { }

            @Override
            public void afterTextChanged(Editable s) {
                if (podemosActualizarResultado())
                    actualizaResultado();
            }
        });
        operando2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) { }

            @Override
            public void afterTextChanged(Editable s) {
                if (podemosActualizarResultado())
                    actualizaResultado();
            }
        });
        botonOcultar.setVisibility(View.GONE);

        historial = (ListView) findViewById(R.id.historial);
        historial.setVisibility(View.GONE);
    }

    private boolean podemosActualizarResultado() {
        return ((this.modelo.getOperador()!="") && (operando1.getText().toString()!="") && (operando2.getText().toString()!=""));
    }
    private void actualizaResultado() {
        int valor = 0,numero1=0,numero2=0;
        boolean valorValido = true;
        try {
            numero1 = Integer.valueOf(operando1.getText().toString());
            numero2 = Integer.valueOf(operando2.getText().toString());
        } catch (NumberFormatException e) {
            valorValido = false;
        }
        if (valorValido) {
            switch (this.modelo.getOperador()) {
                case "*" : valor = numero1 * numero2; break;
                case "/" : valor = numero1 / numero2; break;
                case "+" : valor = numero1 + numero2; break;
                case "-": valor = numero1 - numero2; break;
                default : valorValido = false;
            }
        }

        if (valorValido) {
            resultado.setText( String.valueOf( valor ) );
            modelo.addToHistorial(String.format("%s %s %s = %s",numero1,this.modelo.getOperador(),numero2,valor));
        } else {
            resultado.setText( "" );
        }
    }
    private void cambiaOperador(String operador){
        this.modelo.setOperador(operador);
        if (this.podemosActualizarResultado()) {
            actualizaResultado();
        }
    }

    public void cambiaOperadorMas(View v){
        this.cambiaOperador("+");
    }
    public void cambiaOperadorMenos(View v){
        this.cambiaOperador("-");
    }
    public void cambiaOperadorPor(View v){
        this.cambiaOperador("*");
    }
    public void cambiaOperadorDiv(View v){
        this.cambiaOperador("/");
    }
    public void borrarDatos(View v){
        this.cambiaOperador("");
        resultado.setText("");
        operando1.setText("");
        operando2.setText("");
    }
    public void verHistorial(View v) {
        botonOcultar.setVisibility(View.VISIBLE);
        botonMostrar.setVisibility(View.GONE);
        botonCancelar.setVisibility(View.GONE);
        historial.setVisibility(View.VISIBLE);
    }
    public void ocultarHistorial(View v) {
        botonOcultar.setVisibility(View.GONE);
        botonMostrar.setVisibility(View.VISIBLE);
        botonCancelar.setVisibility(View.VISIBLE);
        historial.setVisibility(View.GONE);
    }
}